# personal-manage

React native feature points:
  1. Write Once and Use Everywhere:  compose complicated UIs of small and isolated pieces of code referred to as “components”.
  2. Programming Language: it uses one of such and popular language is javascipt
  3. UI Focussed: React Native is purely focussed on UI design. It is greatly responsive and its rendering abilities are by far the best.
  4. Community:  React Native’s community is one of the strongest in the cross-platform mobile application development world.
  5. Tried and Trusted: Built by an IT giant like Facebook, it is tried enough to be trusted.
  6. Development Time: The development time in React Native is considerably short.
  7. Support for Third-Party Libraries
  9. NPM for Installation
  10. Mobile Environments Performance
  11. Live Reload:
React native UI trend: 


